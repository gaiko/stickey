SticKey
==============

SticKey is an implementation of an input method for game controllers and other devices
featuring an analogue stick and a set of buttons. It works by the user pushing the
stick in one of eight directions to select a group of characters and then pressing
one of four buttons to select a character from the group. Its layout is based on
qwerty to make use of user familiarity and ease learning to use the input method. 

The intent of SticKey is to allow painless input of small amounts of text when
using a home theatre PC or similar set up. 

![stickey screenshot](media/capture.png)