stickAngle = 0 ;radians, 0 is right
stickMagnitude = 0
;	5 4 3
;	6 1 2
;	7 8 9
currentSegment = 0
symbolMode := false
capsMode := false
lastSymbolMode := false
lastCapsMode := false
modeChanged := false
active := false

PI = 3.141592653
blue = 00B4D5
green = AEE840
red = FF442A
yellow = FEE900

topMargin = 4
sideMargin = 4
buttonSize = 50
horizontalGroupSpacing = 6
verticalGroupSpacing = 6
stagger = 16
;stagger = 0
winHeight := 2 * topMargin + 3 * buttonSize + 2 * verticalGroupSpacing
winWidth := 2 * sideMargin + 9 * buttonSize + 2 * horizontalGroupSpacing + 2 * stagger

segments := [["h", "g", "f", "'"] ;1
	, ["l", "k", "j", """"] ;2
	, ["o", "i", "u", "p"] ;3
	, ["y", "t", "r", ")"] ;4
	, ["e", "w", "q", "("] ;5
	, ["d", "s", "a", ":"] ;6
	, ["c", "x", "z", "/"] ;7
	, ["n", "b", "v", "!"] ;8
	, [".", ",", "m", "?"] ] ;9
	
shiftSegments := [["H", "G", "F", "'"] ;1
	, ["L", "K", "J", """"] ;2
	, ["O", "I", "U", "P"] ;3
	, ["Y", "T", "R", ")"] ;4
	, ["E", "W", "Q", "("] ;5
	, ["D", "S", "A", ":"] ;6
	, ["C", "X", "Z", "/"] ;7
	, ["N", "B", "V", "!"] ;8
	, [".", ",", "M", "?"] ] ;9
	
symSegments := [["*", "&", ";", "'"] ;1
	, ["-", "+", "=", """"] ;2
	, ["\", "}", "{", "_"] ;3
	, ["@", "]", "[", ")"] ;4
	, ["3", "2", "1", "("] ;5
	, ["6", "5", "4", ":"] ;6
	, ["9", "8", "7", "/"] ;7
	, ["$", "#", "0", "!"] ;8
	, [".", ",", "%", "?"] ] ;9


GoSub BuildUI

Return


BuildUI:
	gui, font, s12, Arial

	;gui, add, Picture, x0 y0 w%winWidth% h%winHeight% 0x4000000, stickeyBG1.png

	Loop % 27 {
		k := A_Index
		x := ((mod(k-1, 9)) * buttonSize) + (mod(k-1, 9))//3 * horizontalGroupSpacing

		;y := (k//10) * buttonSize + (k//10) * verticalGroupSpacing

		if (k < 10) {
			y := 0
		} else if (k < 19) {
			y := buttonSize + verticalGroupSpacing
			x := x + stagger
		} else {
			y := 2*buttonSize + 2*verticalGroupSpacing
			x := x + 2 * stagger
		}
		y := y + topMargin
		x := x + sideMargin

		if (mod(k, 3) = 1) {
			gui, add, text, % "x"x " y"y " w"buttonSize*3 " h"buttonSize " BackgroundTrans Border"
		}

		gui, add, text, x%x% y%y% w%buttonSize% h%buttonSize% vBox%k% BackgroundTrans Center, ~
	}
	x := 9 * buttonSize + 2 * horizontalGroupSpacing + sideMargin
	y := topMargin
	;gui, add, text, x%x% y%y% w%buttonSize% h%buttonSize% vBox0 BackgroundTrans Border Center, p

	gui, add, progress, x1000 y1000 w40 h40 vBlueBox Background%blue%
	gui, add, progress, x1000 y1000 w40 h40 vGreenBox Background%green%
	gui, add, progress, x1000 y1000 w40 h40 vYellowBox Background%yellow%
	gui, add, progress, x1000 y1000 w40 h40 vRedBox Background%red%

	GoSub UpdateLabels

	Gui, Show, W%winWidth% H%winHeight% Center, Stickey
	; Gui, hide
	Gui, -Resize -MaximizeBox +AlwaysOnTop
	;Gui +LastFound  ; Make the GUI window the last found window for use by the line below.
	;WinSet, Transparent, 200
	
	SetTimer, UpdateJoystick, 10
return

UpdateLabels:
	if (symbolMode and not capsMode) {
		labels := ["1", "2`n(", "3", "[", "]`n)", "@", "{", "}`n_", "\", "4", "5`n:", "6", ";", "&&`n'", "*", "=", "+`n""", "-", "7", "8`n/", "9", "0", "#`n!", "$", "%", ",`n?", "."]	
	} else if (capsMode and not symbolMode) {
		labels := ["Q", "W`n(", "E", "R", "T`n)", "Y", "U", "I`nP", "O", "A", "S`n:", "D", "F", "G`n'", "H", "J", "K`n""", "L", "Z", "X`n/", "C", "V", "B`n!", "N", "M", ",`n?", "."]
	} else { ;if we've somehow got both, default to neither
		labels := ["q", "w`n(", "e", "r", "t`n)", "y", "u", "i`np", "o", "a", "s`n:", "d", "f", "g`n'", "h", "j", "k`n""", "l", "z", "x`n/", "c", "v", "b`n!", "n", "m", ",`n?", "."]
	}

	for k, v in labels {
		GuiControl, text, Box%k%, %v%
	}
Return

UpdateJoystick:
	if !active {
		;return
	}
	
	lastSymbolMode := symbolMode
	lastCapsMode := capsMode
	GetKeyState, jz, JoyZ
	if (jz > 65) {
		symbolMode := true
		capsMode := false
	} else if (jz < 35) {
		symbolMode := false
		capsMode := true
	} else {
		symbolMode := false
		capsMode := false
	}
	modeChanged := (lastSymbolMode != symbolMode) or (lastCapsMode != capsMode)
	
	GetKeyState, jx, JoyX
	GetKeyState, jy, JoyY
	jx := jx - 50
	jy := jy - 50

	stickAngle := ATan(jy/jx)
	if (jy < 0) {
		if (jx > 0) {
			stickAngle := 0 - stickAngle
		} else {
			stickAngle := PI - stickAngle
		}
	} else {
		if (jx > 0) {
			stickAngle := 2*PI - stickAngle
		} else {
			stickAngle := PI - stickAngle
		}
	}
	stickMagnitude := Sqrt(jx**2 + jy**2)

	lastSegment := currentSegment

	if (stickMagnitude < 25) {
		currentSegment := 1
	} else {
		if (stickAngle < pi*1/8 or stickAngle > pi*15/8) {
			currentSegment := 2
		} else if (stickAngle < pi*3/8) {
			currentSegment := 3
		} else if (stickAngle < pi*5/8) {
			currentSegment := 4
		} else if (stickAngle < pi*7/8) {
			currentSegment := 5
		} else if (stickAngle < pi*9/8) {
			currentSegment := 6
		} else if (stickAngle < pi*11/8) {
			currentSegment := 7
		} else if (stickAngle < pi*13/8) {
			currentSegment := 8
		} else if (stickAngle < pi*15/8) {
			currentSegment := 9
		}
	}

	if (lastSegment != currentSegment) {
		GoSub MoveBackgrounds
	}

	if modeChanged {
		GoSub UpdateLabels
	}
return

MoveBackgrounds:
	size := buttonSize/4
	if (currentSegment = 1) {
		x := sideMargin  + 3 * buttonSize + horizontalGroupSpacing + stagger
		y := topMargin + buttonSize + verticalGroupSpacing

	} else if (currentSegment = 2) {
		x := sideMargin  + 6 * buttonSize + 2 * horizontalGroupSpacing + stagger
		y := topMargin + buttonSize + verticalGroupSpacing

	} else if (currentSegment = 3) {
		x := sideMargin + 6 * buttonSize + 2 * horizontalGroupSpacing
		y := topMargin

	} else if (currentSegment = 4) {
		x := sideMargin + 3 * buttonSize + horizontalGroupSpacing
		y := topMargin

	} else if (currentSegment = 5) {
		x := sideMargin
		y := topMargin

	} else if (currentSegment = 6) {
		x := sideMargin
		y := topMargin + buttonSize + verticalGroupSpacing

	} else if (currentSegment = 7) {
		x := sideMargin + 2 * stagger
		y := topMargin + 2 * buttonSize + 2 * verticalGroupSpacing

	} else if (currentSegment = 8) {
		x := sideMargin + 3 * buttonSize + horizontalGroupSpacing + 2 * stagger
		y := topMargin + 2 * buttonSize + 2 * verticalGroupSpacing

	} else if (currentSegment = 9) {
		x := sideMargin + 6 * buttonSize + 2 * horizontalGroupSpacing + 2 * stagger
		y := topMargin + 2 * buttonSize + 2 * verticalGroupSpacing

	}

	y := y + 3
	x := x + 3

	GuiControl, move, BlueBox, x%x% y%y% w%size% h%size%
	x := x + buttonSize
	GuiControl, move, YellowBox, x%x% y%y% w%size% h%size%
	GuiControl, move, GreenBox, % "x"x " y"y+buttonSize/2 " w"size " h"size
	x := x + buttonSize
	GuiControl, move, RedBox, x%x% y%y% w%size% h%size%
Return

;   2
; 3   1
;   4
SendLetter(which) {
	global currentSegment
	
	global segments
	global symSegments
	global shiftSegments
	
	global symbolMode
	global capsMode
	
	if (symbolMode and not capsMode) {
		segmentSet := symSegments
	} else if (capsMode and not symbolMode) {
		segmentSet := shiftSegments
	} else { ;if we've somehow got both, default to neither
		segmentSet := segments
	}

	SendRaw % segmentSet[currentSegment][which]
}

^!r::Reload

Joy2::
	if active {
		SendLetter(1)
	}
Return

Joy1::
	if active {
		SendLetter(4)
	}
Return

Joy3::
	if active {
		SendLetter(3)
	}
Return

Joy4::
	if active {
		SendLetter(2)
	}
Return

Joy5::
	if active {
		Send {BackSpace}
	}
Return

Joy6::
	if active {
		Send {Space}
	}
Return

Joy7::
	if active {
		active := false
		Gui, Hide
	} else {
		active := true
		Gui, Show, NoActivate
	}
Return

GuiClose:
	ExitApp
Return